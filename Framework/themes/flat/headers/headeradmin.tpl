<!DOCTYPE HTML>
	<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ Titleshort }}</title>
		<!--<link rel='stylesheet prefetch' href='http:////netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>-->
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}paneladmin.css"/>
		<!--<link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">-->
		<script type="text/javascript" src="{{ THEME_JS_URL }}jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="{{ THEME_JS_URL }}bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}monokai-sublime.css"/>
		<script type="text/javascript" src="{{ THEME_JS_URL }}highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<nav class="navbar navbar-default" id="headerBlue">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div id="logo">
						<span>
							<a class="navbar-brand" href="{{ URL }}">
							<i class="fa fa-heart fa-3x" aria-hidden="true"></i> {{ Titleshort }}</a>
						</span>
					</div>
				</div>

				<div id="navegation">
					<ul>

						{% for itemMenuH, itemMenuC in data.menu %}

							{% set act = '' %}
							{% if data.section == itemMenuH %}
								{% set act = 'class=active' %}
							{% endif %}
							<li {{ act }}><a href="{{ itemMenuC.1 }}">{{ itemMenuC.0 }}</a></li>
						{% endfor %}
					</ul>
				</div>

				<div class="userinfo verticalAlign">
					<a href="{{ URL }}" title="{{ lang.return }}">
						<img class="avatar" src="{{ THEME_IMAGE_URL }}avatars/{{ data.u_avatar }}">
						<span class="username">{{ data.u_user }}</span>
					</a>
				</div>
			</div>
		</nav>

		<main class="container">