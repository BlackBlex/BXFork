<!DOCTYPE HTML>
	<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ Titleshort }}</title>
		<!--<link rel='stylesheet prefetch' href='http:////netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>-->
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}socialicon.css"/>
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}style.css"/>
		<!--<link href="https://fonts.googleapis.com/css?family=Questrial" rel="stylesheet">-->
		<script type="text/javascript" src="{{ THEME_JS_URL }}jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="{{ THEME_JS_URL }}bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}monokai-sublime.css"/>
		<script type="text/javascript" src="{{ THEME_JS_URL }}highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<header>
			<div id="header-wrapper">
				<div id="header" class="container">
					<div id="logo">
						<i class="fa fa-heart fa-3x" aria-hidden="true"></i>
						<h1>
							<a href="{{ URL }}">{{ Titleshort }}</a>
						</h1>
					</div>
					<div id="navegation">
						<ul>
							{% include 'menu' ignore missing %}
						</ul>
					</div>
				</div>
			</div>
		</header>

		<main class="container">