
			<div class="separator">
				<div class="line"></div>
			</div>
		</main>
		<div id="sociallink">
			<ul class="social-icons icon-circle icon-zoom list-unstyled list-inline"> 
				<li> <a href="https://bitbucket.org/BlackBlex/"><i class="fa fa-bitbucket"></i></a> </li> 
				<li> <a href="https://www.facebook.com/profile.php?id=100011197696318"><i class="fa fa-facebook"></i></a></li>
				<li> <a href="https://plus.google.com/u/0/114730628309351873457"><i class="fa fa-google-plus"></i></a></li> 
				<li> <a href="https://instagram.com/BlackBlex"><i class="fa fa-instagram"></i></a></li>
				<li> <a href="skype:BlackBlex?call"><i class="fa fa-skype"></i></a></li>
				<li> <a href="https://twitter.com/BlackBlex"><i class="fa fa-twitter"></i></a></li>
				<li> <a href="https://www.youtube.com/user/BlackBlex"><i class="fa fa-youtube-play"></i></a></li>
			</ul>

		</div>

		<footer>
			<div id="footer-wrapper">

				{% if data.u_id == 1 %}
					<center>
						<h3 class="panelAdmin"><a href="{{ getUrl('admin','login') }}">[{{ langGlobal.paneladmin }}]</a></h3>
					</center>
				{% endif %}

				<div id="footer" class="container">
					<div id="theme">
						<span>
							{{ langGlobal.theme }} 
						</span>
						<ul>
							{% for theme in themes %}
								<li><a href="{{ URL }}theme/{{ theme }}">{{ theme }}</a></li>
							{% endfor %}
						</ul>
					</div>
				</div>
			</div>
			<!--
				<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licencia de Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Framework Basico</span> by <span xmlns:cc="http://creativecommons.org/ns#" property="cc:attributionName">Jovani Pérez (BlackBlex)</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Reconocimiento-NoComercial-CompartirIgual 4.0 Internacional License</a>.
			-->
		</footer>
	</body>
</html>