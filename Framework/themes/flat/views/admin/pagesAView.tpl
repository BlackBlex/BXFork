{% include getVal("_header") ~ '.tpl' %}
		<div class="row">
			<div class="col-md-12 box_trans">
				<div class="col-md-2 box_trans">
				</div>

				<div class="col-md-8 box_trans">
					<div class="col-md-12 box_trans">
						<div class="box_lightgreen">
							<div class="verticalAlign">
								<i class="fa fa-file fa-4x" aria-hidden="true"></i>
								<div class="verticalLine">
									<span class="box_title">{{ lang.add }}</span>
									<br>
									<span class="box_subtitle">{{ data.controller }}</span>
								</div>
							</div>
						</div>
						<div class="box_border_lightgreen">
							<form class="form-horizontal" _lpchecked="1" action="{{ getUrl('admin', 'pages/add') }}" method="post">
								<fieldset>
									<input type="text" hidden="true" value="{{ data.controller }}" id="controller" name="controller">
									<div class="form-group size-17">
										<label for="namepage" class="col-lg-4 control-label">{{ lang.addname }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="namepage" name="namepage" placeholder="{{ lang.addname }}" autocomplete="off">
										</div>
									</div>
									<div class="form-group size-17">
										<label for="parmpage" class="col-lg-4 control-label">{{ lang.addparms }}</label>
										<div class="col-lg-8">
											<input type="text" class="form-control" id="parmpage" name="parmpage" placeholder="{{ lang.addparms }}" autocomplete="off">
											<span class="help-block">{{ lang.addparms1 }}</span>
										</div>
									</div>
									<div class="form-group">
										<center>
											<button type="submit" id="save" name="save" class="btn btn-primary btn-lg">{{ lang.savechanges }}</button>
										</center>
									</div>
								</fieldset>
							</form>
						</div>
					</div>
				</div>

				<div class="col-md-2 box_trans">
				</div>
			</div>
		</div>
{% include getVal("_footer") ~ '.tpl' %}