<!DOCTYPE HTML>
	<html lang="es">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>{{ Titleshort }}</title>
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}bootstrap.css" />
		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}style.css"/>
		<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'> -->
		<script type="text/javascript" src="{{ THEME_JS_URL }}jquery-3.1.0.min.js"></script>
		<script type="text/javascript" src="{{ THEME_JS_URL }}bootstrap.min.js"></script>

		<link rel="stylesheet" type="text/css" href="{{ THEME_CSS_URL }}monokai-sublime.css"/>
		<script type="text/javascript" src="{{ THEME_JS_URL }}highlight.pack.js"></script>
		<script>hljs.initHighlightingOnLoad();</script>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-default">
  				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#items_de_navbar">
							<span class="sr-only">Navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
      					<a class="navbar-brand title" href="{{ URL }}">{{ Titleshort }}</a>
					</div>
					<div class="collapse navbar-collapse navbar-right" id="items_de_navbar">
						<ul class="nav navbar-nav">
							{% include 'menu' ignore missing %}
							<li>
								<div class="dropdown" style="margin-top: 10px;">
									<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									{{ langGlobal.theme }}
									<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
										{% for theme in themes %}
											<li><a href="{{ URL }}theme/{{ theme }}">{{ theme }}</a></li>
										{% endfor %}
									</ul>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<main class="container">