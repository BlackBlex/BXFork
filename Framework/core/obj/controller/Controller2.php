<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Controller
 *
 * ==============Information==============
 *      Filename: Controller2.php
 *          Path: ./core/obj/controller/
 * ---------------------------------------
*/

trait Controller2
{
	public $c_controller;
	public $c_action;
	public $c_args;
	public $c_limit = true;
	public $c_timeEnd;

	/**
	 * Función constructora del controlador.
	 * @param string $controller 
	 * @param string $action 
	 * @param Array $args 
	 * @return null
	 */
	public function __construct($controller, $action, $args)
	{
		$this->c_controller = $controller;
		$this->c_action = $action;
		$this->c_args = $args;
	}

	/**
	 * Esta función se ejecuta antes de realizar la carga del controlador.
	 * @return null
	 */
	protected function before()
	{
	}

	/**
	 * Esta función se ejecuta despues de realizar la carga del controlador.
	 * @return null
	 */
	protected function after()
	{

	}

	/**
	 * Esta función se ejecuta con la carga del controlador.
	 * @return null
	 */
	protected function start()
	{

	}

	/**
	 * Esta función se ejecuta al final de realizar la carga del controlador.
	 * @return null
	 */
	protected function end()
	{
		$this->c_timeEnd = number_format((microtime(true) - START), 2, '.', '');
	}

	/**
	 * Redirecciona en alguna parte en concreto.
	 * @param string $controller 
	 * @param string $action 
	 * @return null
	 */
	public function redirect($controller=false, $action=false)
	{
		global $_Variables;
		if ( !$controller )
			$controller = $_Variables->getVar('config')['options']['defaultController'];

		if ( !$action )
			$action = $_Variables->getVar('config')['options']['defaultAction'];

		echo "<script type='text/javascript'> document.location = '" . $_Variables->getVar('config')['site']['url'] . "/" . $controller . "/" . $action . "'; </script>";
	}

	/**
	 * Esta funcion es la carga del controlador.
	 * @param boolean $init 
	 * @return null
	 */
	final public function load($init = FALSE)
	{
		if ($init)
		{
			if ($this->start() !== FALSE)
				return $this->before();
			return FALSE;
		}

		$this->after();
		$this->end();
	}

}

?>