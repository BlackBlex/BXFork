<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Database
 *
 * ==============Information==============
 *      Filename: Database.php
 *          Path: ./core/obj/database/
 * ---------------------------------------
*/

trait Database
{

	/**
	 * Nombre de la tabla la cual se utilizara en las consultas
	 * @var string
	 * @access private
	 */
	private $_table;

	/**
	 * Para la realización de las consultas
	 * @var mysqli
	 * @access private
	 */
	private $_db;

	/**
	 * Contiene la conexión a la base de datos
	 * @var Connection
	 * @access private
	 */
	private $_connect;

	/**
	 * Inicia conexión a la base de datos
	 * @return null
	 */
	public function DBinit()
	{
		global $allFunctions;

		$this->_connect = new Connection();
		$this->_db = $this->_connect->connection();
	}

	/**
	 * Establece la tabla que se usa en las consultas
	 * 
	 * Se utiliza mayormente en los models,
	 * ya que en cada model se debe especificar la tabla a usar.
	 * 
	 * @param String $table Nombre de la tabla a usar. 
	 * @return null
	 */
	public function setTable($table)
	{
		$this->_table = $table;
	}

	/**
	 * Obtiene la conexión
	 * @return Connection
	 */
	public function getConnect()
	{
		return $this->_connect;
	}

	/**
	 * Obtiene la conexión lista para realizar consultas
	 * @return mysqli
	 */
	public function db()
	{
		return $this->_db;
	}

	/**
	 * Obtiene todos los registros de la tabla
	 * @return array
	 */
	public function getAll()
	{
		$query = $this->_db->query("SELECT * FROM $this->_table");

		while ( $row = $query->fetch_object() )
		{
			$resultSet[] = $row;
		}

		return $resultSet;
	}

	/**
	 * Obtiene el registro de una tabla mediante el campo "id"
	 * 
	 * Se obtiene el registro de una tabla especificando el ID y la columna
	 * 
	 * @param string $column El campo a obtener
	 * @param integer $id El ID del registo del cual quieres obtener el campo
	 * @return string Retorna el campo a obtener si existe el ID, si no retorna "nothing" y si el ID pasado no es int retorna "Value not is numeric."
	 */
	public function getById($column, $id)
	{
		if ( is_numeric($id) )
		{
			$query = $this->_db->query("SELECT $column FROM $this->_table WHERE id=$id");

			$resultSet = $query->fetch_assoc();

			if ( empty($resultSet[$column]))
				$resultSet[$column] = 'nothing';

			return $resultSet[$column];
		}
		errorFunction(31, "Database processing failed");
		return 'Value not is numeric.';
	}

	/**
	 * Obtiene el registro de una tabla mediante un campo
	 * 
	 * Se obtiene el registro completo de una tabla especificando el campo y el valor
	 * 
	 * @param string $column El campo del cual quieres condicionar
	 * @param string $value El valor del campo
	 * @return array
	 */
	public function getBy($column, $value)
	{
		$query = $this->_db->query("SELECT * FROM $this->_table WHERE $column='$value'");

		while ( $row = $query->fetch_object() )
		{
			$resultSet[] = $row;
		}

		return $resultSet[0];
	}

	/**
	 * Elimina un registro mediante el campo "id"
	 * @param integer $id 
	 * @return boolean
	 */
	public function deleteById($id)
	{
		if ( is_numeric($id) )
		{
			$query=$this->_db->query("DELETE FROM $this->_table WHERE id=$id");
			return $query;
		}
		errorFunction(32, "Database processing failed");
		return 'Value not is numeric.';
	}

	/**
	 * Elimina un registro mediante el campo que indiques
	 * @param string $column 
	 * @param string $value 
	 * @return boolean
	 */
	public function deleteBy($column, $value)
	{
		$query=$this->_db->query("DELETE FROM $this->_table WHERE $column='$value'");
		return $query;
	}


	public function execSql($query)
	{
		$query = $this->_db->query($query);

		if ( true == $query )
		{
			if ( $query->num_rows > 1 )
			{
				while ( $row = $query->fetch_object() )
					$resultSet[] = $row;
			}
			elseif ( $query->num_rows == 1 )
			{
				if ($row = $query->fetch_object())
					$resultSet = $row;

			}
			else
				$resultSet = true;

		}
		else
			$resultSet = false;

		return $resultSet;

	}

}

?>