<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Obj
 * @subpackage  Database
 *
 * ==============Information==============
 *      Filename: Connection.php
 *          Path: ./core/obj/database/
 * ---------------------------------------
*/

class Connection
{
	private $_driver;

	/**
	 * Host de la base de datos
	 * @var string
	 * @access private
	 */
	private $_host;

	/**
	 * Usuario de la base de datos
	 * @var string
	 * @access private
	 */
	private $_user;

	/**
	 * Contraseña de la base de datos
	 * @var string
	 * @access private
	 */
	private $_pass;

	/**
	 * Nombre de la base de datos
	 * @var string
	 * @access private
	 */
	private $_database;

	/**
	 * Charset usada en la base de datos
	 * @var string
	 * @access private
	 */
	private $_charset;

	public function __construct() 
	{
		global $_Variables;
		$this->_driver=$_Variables->getVar('config')['database']['driver'];
		$this->_host=$_Variables->getVar('config')['database']['server'];
		$this->_user=$_Variables->getVar('config')['database']['user'];
		$this->_pass=$_Variables->getVar('config')['database']['pass'];
		$this->_database=$_Variables->getVar('config')['database']['name'];
		$this->_charset=$_Variables->getVar('config')['database']['char'];

	}

	/**
	 * Realiza la conexión a la base de datos
	 * @return null
	 */
	public function connection()
	{
		if ( $this->_driver=='mysql' || $this->_driver==null )
		{
			try
			{
				$con = new mysqli($this->_host, $this->_user, $this->_pass, $this->_database);
				$con->query("SET NAMES '" . $this->_charset . "'");
			}
			catch(Exception $e)
			{
				throw new Exception("Database processing failed", 30);
			}
				
		}
		return $con;
	}

}

?>