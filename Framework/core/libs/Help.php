<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	Libs
 *
 * ==============Information==============
 *      Filename: Help.php
 *          Path: ./core/libs/
 * ---------------------------------------
*/

class Help
{
	private $_models = [];

	/**
	 * Cargamos un modelo y lo guardamos en una variable para su uso posterior
	 * @param string $model Nombre del modelo (Debe respetar mayusculas y minusculas)
	 * @param array $params Si el modelo requiere algun parametro para ser lanzado, debes incluirlas como array
	 * @return null
	 */
	public function loadModel($model, $params = NULL)
	{
		$Model = $model;
		if (!class_exists($Model, FALSE))
			if (require_once MODELS_PATH . "$model.php")
				$this->_models[$model] = new $Model($params);

	}

	/**
	 * Retorna el modelo para ser usado.
	 * @param string $model Nombre del modelo (Debe respetar mayusculas y minusculas)
	 * @return Model
	 */
	public function model($model)
	{
		if ( isset($this->_models[$model]) )
			return $this->_models[$model];
		return NULL;
	}

	/**
	 * Obtiene el hass de algun texto.
	 * @param string $text 
	 * @return string
	 */
	public function Hash($text)
	{
		$hash = password_hash($text, PASSWORD_DEFAULT);
		return $hash;
	}

	public function saveFile($path, $text)
	{
		if ( is_writable($path) && is_file($path) )
		{
			file_put_contents($path, $text);
			return true;
		}
		else
			return false;
	}

}

?>