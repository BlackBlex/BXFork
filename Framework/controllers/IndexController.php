<?php
/**
 * BXFork
 *
 * Framework basico para el desarrollo web.
 *
 * @author 		Jovani Pérez (@BlackBlex)
 * @license 	General Public License (GPLv3) | http://www.gnu.org/licenses/
 * @package 	controllers
 *
 * ==============Information==============
 *      Filename: IndexController.php
 *          Path: ./controllers/
 * ---------------------------------------
*/

class IndexController extends Controller
{

	public function index()
	{
		if ( isset($_SESSION["u_user"]) )
		{
			$this->u_id = $_SESSION["u_id"];
			$this->u_user = $_SESSION["u_user"];
		}
		else
		{
			$this->u_user = "Visitante";
		}
	
	}

	public function about($test)
	{
		$this->test = $test;
	}
	
	public function theme($theme)
	{
		$_SESSION['u_theme'] = $theme;
		$this->redirect();
		
	}

}
?>